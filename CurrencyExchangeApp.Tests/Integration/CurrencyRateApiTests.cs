using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CurrencyExchangeApp.ViewModels;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace CurrencyExchangeApp.Tests.Integration
{
    public class CurrencyRateApiTests
        : IClassFixture<CurrencyExchangeAppFactory<CurrencyExchangeApp.Startup>>
    {
        private readonly HttpClient _client;

        public CurrencyRateApiTests(
            CurrencyExchangeAppFactory<CurrencyExchangeApp.Startup> factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });
        }

        [Fact]
        public async Task GetCurrencyRate_WhenValidCurrencyCode_ReturnsHttpOk()
        {
            // Arrange
            var route = "/api/v1/currencies/SUC/rates";

            // Act
            var response = await _client.GetAsync(route);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenValidCurrencyCode_ReturnsRateInformation()
        {
            // Arrange
            var route = "/api/v1/currencies/SUC/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<CurrencyRateModel>();

            // Assert
            Assert.Equal("2", responseModel.Data.Buy);
            Assert.Equal("3", responseModel.Data.Sell);
            Assert.True(DateTime.UtcNow >= responseModel.Data.LastUpdateTimestamp);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenValidCurrencyCode_ReturnsMetadata()
        {
            // Arrange
            var route = "/api/v1/currencies/SUC/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<CurrencyRateModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenCurrencyCodeIrregularCased_IsCaseInsensitive()
        {
            // Arrange
            var route = "/api/v1/currencies/sUc/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<CurrencyRateModel>();

            // Assert
            Assert.Equal("2", responseModel.Data.Buy);
            Assert.Equal("3", responseModel.Data.Sell);
            Assert.True(DateTime.UtcNow >= responseModel.Data.LastUpdateTimestamp);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenErrorCurrencyCode_ReturnsHttpInternalServerError()
        {
            // Arrange
            var route = "/api/v1/currencies/ERR/rates";

            // Act
            var response = await _client.GetAsync(route);

            // Assert
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenErrorCurrencyCode_ReturnsMessage()
        {
            // Arrange
            var route = "/api/v1/currencies/ERR/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.Data.Message);
            Assert.Equal("RATE_EXCEPTION", responseModel.Data.Message);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenErrorCurrencyCode_ReturnsMetadata()
        {
            // Arrange
            var route = "/api/v1/currencies/ERR/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenUnsupportedCurrencyCode_ReturnsHttpNotFound()
        {
            // Arrange
            var route = "/api/v1/currencies/UNK/rates";

            // Act
            var response = await _client.GetAsync(route);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenUnsupportedCurrencyCode_ReturnsMessage()
        {
            // Arrange
            var route = "/api/v1/currencies/UNK/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.Data.Message);
            Assert.Equal("Currency code \"UNK\" not supported.", responseModel.Data.Message);
        }

        [Fact]
        public async Task GetCurrencyRate_WhenUnsupportedCurrencyCode_ReturnsMetadata()
        {
            // Arrange
            var route = "/api/v1/currencies/UNK/rates";

            // Act
            var responseModel = await _client.GetAsync(route).ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }
    }
}