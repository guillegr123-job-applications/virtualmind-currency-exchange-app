using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CurrencyExchangeApp.ViewModels;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace CurrencyExchangeApp.Tests.Integration
{
    public class CurrencyTransactionApiTests
        : IClassFixture<CurrencyExchangeAppFactory<CurrencyExchangeApp.Startup>>
    {
        private readonly HttpClient _client;
        private static readonly string _transactionsUrl = "/api/v1/transactions";
        private static readonly string _buyTransactionUrl = $"{_transactionsUrl}/buy";
        private static readonly string _unsupportedTransactionUrl = $"{_transactionsUrl}/unsupported";

        public CurrencyTransactionApiTests(
            CurrencyExchangeAppFactory<CurrencyExchangeApp.Startup> factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false
                });
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenValid_ReturnsHttpOk()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 3,
                    CurrencyCode = "SUC"
                });

            // Act
            var response = await _client.PutAsync(_buyTransactionUrl, transactionPayload);

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenValid_ReturnsTransactionInformation()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "SUC"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<CurrencyTransactionResponseModel>();
            
            // Assert
            Assert.NotEmpty(responseModel.Data.Id);
            Assert.Equal("1", responseModel.Data.UserId);
            Assert.Equal(6, responseModel.Data.Amount);
            Assert.Equal("SUC", responseModel.Data.CurrencyCode);
            Assert.Equal("3", responseModel.Data.Rate);
            Assert.Equal(2, responseModel.Data.Value);
            Assert.True(DateTime.UtcNow >= responseModel.Data.Timestamp);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenValid_ReturnsMetadata()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 3,
                    CurrencyCode = "SUC"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<CurrencyTransactionResponseModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenCurrencyCodeIrregularCased_IsCaseInsensitive()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "SuC"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<CurrencyTransactionResponseModel>();

            // Assert
            Assert.NotEmpty(responseModel.Data.Id);
            Assert.Equal("1", responseModel.Data.UserId);
            Assert.Equal(6, responseModel.Data.Amount);
            Assert.Equal("SUC", responseModel.Data.CurrencyCode);
            Assert.Equal("3", responseModel.Data.Rate);
            Assert.Equal(2, responseModel.Data.Value);
            Assert.True(DateTime.UtcNow >= responseModel.Data.Timestamp);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenCurrencyHasNoLimit_ReturnsHttpNotImplementedWithMessage()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 3,
                    CurrencyCode = "NLI"
                });

            // Act
            var response = await _client.PutAsync(_buyTransactionUrl, transactionPayload);
            var responseModel = await response.ToResponseModel<ErrorModel>();

            // Assert
            Assert.Equal(HttpStatusCode.NotImplemented, response.StatusCode);
            Assert.NotEmpty(responseModel.Data.Message);
            Assert.Equal(
                "The transaction cannot be applied, because no limits have been established for \"NLI\"",
                responseModel.Data.Message
            );
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenCurrencyHasNoLimit_ReturnsMetadata()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 3,
                    CurrencyCode = "NLI"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenMonthlyLimitSurpassed_ReturnsHttpTooManyRequestsWithMessage()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "2",
                    Amount = 6,
                    CurrencyCode = "LIM"
                });

            // Act
            var response = await _client.PutAsync(_buyTransactionUrl, transactionPayload);
            var responseModel = await response.ToResponseModel<ErrorModel>();

            // Assert
            Assert.Equal(HttpStatusCode.TooManyRequests, response.StatusCode);
            Assert.NotEmpty(responseModel.Data.Message);
            Assert.Equal(
                "The transaction cannot be applied, because it will exceed the monthly user"
                    + " limit for \"Buy\" transactions for LIM."
                    + "\nLIM 1 of 5 remaining.",
                responseModel.Data.Message
            );
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenMonthlyLimitSurpassed_ReturnsMetadata()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "2",
                    Amount = 6,
                    CurrencyCode = "LIM"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenErrorCurrencyCode_ReturnsHttpInternalServerError()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "ERR"
                });

            // Act
            var response = await _client.PutAsync(_buyTransactionUrl, transactionPayload);

            // Assert
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenErrorCurrencyCode_ReturnsMessage()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "ERR"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.Data.Message);
            Assert.Equal("RATE_EXCEPTION", responseModel.Data.Message);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenErrorCurrencyCode_ReturnsMetadata()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "ERR"
                });

            // Act
            var responseModel = await _client.PutAsync(_buyTransactionUrl, transactionPayload)
                .ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenUnsupportedTransaction_ReturnsHttpNotFound()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "SUC"
                });

            // Act
            var response = await _client.PutAsync(_unsupportedTransactionUrl, transactionPayload);

            // Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenUnsupportedTransaction_ReturnsMessage()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "SUC"
                });

            // Act
            var responseModel = await _client.PutAsync(_unsupportedTransactionUrl, transactionPayload)
                .ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.Data.Message);
            Assert.Equal("Transaction type \"unsupported\" not found.", responseModel.Data.Message);
        }

        [Fact]
        public async Task PutCurrencyTransaction_WhenUnsupportedTransaction_ReturnsMetadata()
        {
            // Arrange
            var transactionPayload = Utils.buildTransactionStringContent(
                new CurrencyTransactionRequestModel
                {
                    UserId = "1",
                    Amount = 6,
                    CurrencyCode = "SUC"
                });

            // Act
            var responseModel = await _client.PutAsync(_unsupportedTransactionUrl, transactionPayload)
                .ToResponseModel<ErrorModel>();

            // Assert
            Assert.NotEmpty(responseModel.RequestId);
            Assert.True(DateTime.UtcNow >= responseModel.Timestamp);
        }
    }
}