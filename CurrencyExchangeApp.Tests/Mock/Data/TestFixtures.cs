using System;
using System.Linq;
using CurrencyExchangeApp.Data.Models;
using CurrencyExchangeApp.Data;

namespace CurrencyExchangeApp.Tests.Mock.Data
{
    public static class TestFixtures
    {
        public static void Load(AppDbContext dbContext)
        {
            if (dbContext.TransactionLimits.Count() > 0)
            {
                return;
            }

            dbContext.TransactionLimits.AddRange(
                new TransactionLimit
                {
                    CurrencyCode = "SUC",
                    Type = TransactionType.Buy,
                    MontlyUserLimit = 100
                },
                new TransactionLimit
                {
                    CurrencyCode = "LIM",
                    Type = TransactionType.Buy,
                    MontlyUserLimit = 5
                }
            );

            dbContext.CurrencyTransactions.Add(
                new CurrencyTransaction
                {
                    Type = TransactionType.Buy,
                    UserId = 2,
                    CurrencyCode = "LIM",
                    Amount = 12,
                    Rate = 3,
                    Value = 4,
                    Timestamp = DateTime.UtcNow
                }
            );

            dbContext.SaveChanges();
        }
    }
}