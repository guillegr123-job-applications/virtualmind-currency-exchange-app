using System;
using System.Threading.Tasks;
using CurrencyExchangeApp.Services.Interfaces;
using CurrencyExchangeApp.Services.Models;

namespace CurrencyExchangeApp.Tests.Mock.Services
{
    public class MockErrorCurrencyRateService : ICurrencyRateService
    {
        public string[] CurrencyCodes => new string[] { "ERR" };

        public Task<ServiceCurrencyRateModel> GetRatesAsync(string currencyCode)
        {
            throw new Exception("RATE_EXCEPTION");
        }
    }
}
