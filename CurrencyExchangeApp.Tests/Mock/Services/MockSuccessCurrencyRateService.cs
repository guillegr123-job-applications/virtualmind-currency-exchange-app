using System;
using System.Threading.Tasks;
using CurrencyExchangeApp.Services.Interfaces;
using CurrencyExchangeApp.Services.Models;

namespace CurrencyExchangeApp.Tests.Mock.Services
{
    public class MockSuccessCurrencyRateService : ICurrencyRateService
    {
        public string[] CurrencyCodes => new string[] { "SUC", "LIM", "NLI" };

        public Task<ServiceCurrencyRateModel> GetRatesAsync(string currencyCode)
            => Task.FromResult(new ServiceCurrencyRateModel
            {
                Buy = 2,
                Sell = 3,
                LastUpdateTimestamp = DateTime.UtcNow
            });
    }
}
