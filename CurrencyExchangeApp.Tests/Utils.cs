using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using CurrencyExchangeApp.ViewModels;

namespace CurrencyExchangeApp.Tests
{
    internal static class Utils
    {
        internal static StringContent buildTransactionStringContent<T>(T data)
            where T: IViewModel
        {
            var transactionPayloadAsString = JsonConvert.SerializeObject(
                new RequestDataModel<T>
                {
                    Data = data
                });
            return new StringContent(transactionPayloadAsString, Encoding.Default, "application/json");
        }

        internal static async Task<ResponseDataModel<T>> ToResponseModel<T>(this Task<HttpResponseMessage> responseMessage)
            where T: IViewModel
        {
            var response = await responseMessage;
            return await response.ToResponseModel<T>();
        }

        internal static async Task<ResponseDataModel<T>> ToResponseModel<T>(this HttpResponseMessage response)
            where T: IViewModel
        {
            var responseBodyAsSting = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<ResponseDataModel<T>>(responseBodyAsSting);
        }
    }
}