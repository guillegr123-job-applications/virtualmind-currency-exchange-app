using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace CurrencyExchangeApp.Middleware
{
    public class ApiRequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ApiRequestResponseLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory
                .CreateLogger<ApiRequestResponseLoggingMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            await LogRequest(context);
            await LogResponse(context);
        }

        private async Task LogRequest(HttpContext context)
        {
            if (!context.Request.Path.StartsWithSegments("/api"))
            {
                return;
            }

            var logEvent = new ExtendedLogEvent("Request incoming");

            context.Request.EnableBuffering();

            using (var requestStream = new MemoryStream())
            {
                await context.Request.Body.CopyToAsync(requestStream);
                logEvent.WithProperty("scheme", context.Request.Scheme)
                    .WithProperty("host", context.Request.Host)
                    .WithProperty("path", context.Request.Path)
                    .WithProperty("method", context.Request.Method)
                    .WithProperty("qs", context.Request.QueryString.ToString())
                    .WithProperty("body", ReadStreamInChunks(requestStream));
            }
            _logger.Log(Microsoft.Extensions.Logging.LogLevel.Debug,
                1606386230,
                logEvent,
                (Exception)null,
                ExtendedLogEvent.Formatter);
                
            context.Request.Body.Position = 0;
        }

        private async Task LogResponse(HttpContext context)
        {
            if (!context.Request.Path.StartsWithSegments("/api"))
            {
                await _next(context);
                return;
            }

            var logEvent = new ExtendedLogEvent("Response outgoing");
            var originalBodyStream = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;
                await _next(context);
                context.Response.Body.Seek(0, SeekOrigin.Begin);
                var text = await new StreamReader(context.Response.Body).ReadToEndAsync();
                context.Response.Body.Seek(0, SeekOrigin.Begin);
                logEvent.WithProperty("body", text)
                    .WithProperty("status", context.Response.StatusCode);
                _logger.Log(Microsoft.Extensions.Logging.LogLevel.Debug,
                    1606386231,
                    logEvent,
                    (Exception)null,
                    ExtendedLogEvent.Formatter);
                await responseBody.CopyToAsync(originalBodyStream);
            }
        }

        private static string ReadStreamInChunks(Stream stream)
        {
            const int readChunkBufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var readChunk = new char[readChunkBufferLength];
            int readChunkLength;
            do
            {
                readChunkLength = reader.ReadBlock(readChunk, 0, readChunkBufferLength);
                textWriter.Write(readChunk, 0, readChunkLength);
            } while (readChunkLength > 0);
            return textWriter.ToString();
        }
    }
}
