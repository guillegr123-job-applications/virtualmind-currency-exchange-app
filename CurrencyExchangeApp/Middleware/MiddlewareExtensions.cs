using Microsoft.AspNetCore.Builder;

namespace CurrencyExchangeApp.Middleware
{
    internal static class MiddlewareExtensions
    {
        internal static IApplicationBuilder UseApiRequestResponseLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ApiRequestResponseLoggingMiddleware>();
        }
    }
}
