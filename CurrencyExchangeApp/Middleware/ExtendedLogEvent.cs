using System;
using System.Collections;
using System.Collections.Generic;

namespace CurrencyExchangeApp.Middleware
{
    public class ExtendedLogEvent : IEnumerable<KeyValuePair<string, object>>
    {
        List<KeyValuePair<string, object>> _properties = new List<KeyValuePair<string, object>>();

        public string Message { get; }
                    
        public ExtendedLogEvent(string message)
        {
            Message = message;
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return _properties.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

        public ExtendedLogEvent WithProperty(string name, object value)
        {
            _properties.Add(new KeyValuePair<string, object>(name, value));
            return this;
        }

        public static Func<ExtendedLogEvent, Exception, string> Formatter { get; } = (l, e) => l.Message;
    }
}
