import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient, @Inject('BASE_URL') private baseUrl: string) { }

  public getCurrencyRate(currencyCode: string) {
    return this.httpClient.get<ApiResponse<CurrencyRateData>>(`${this.baseUrl}api/v1/currencies/${currencyCode}/rates`);
  }

  public putCurrencyTransaction(type: TransactionType, data: CurrencyTransactionRequestData) {
    const body: ApiRequestBody<CurrencyTransactionRequestData> = {
      data
    };
    return this.httpClient.put<ApiResponse<CurrencyTransactionResponseData>>(`${this.baseUrl}api/v1/transactions/${type}`, body);
  }
}

export interface RequestData {}

export interface ResponseData {}

export interface CurrencyRateData extends ResponseData {
  buy: string;
  sell: string;
}

export interface CurrencyTransactionRequestData extends RequestData {
  userId: string;
  currencyCode: string;
  amount: number;
}

export interface CurrencyTransactionResponseData extends CurrencyTransactionRequestData {
  id: string;
  rate: string;
  value: number;
  timestamp: string;
}

export interface ErrorMessageData { message: string }

export interface ApiResponse<T extends ResponseData> {
  data: T;
  timestamp: string;
}

export interface ApiRequestBody<T extends RequestData> {
  data: T;
}

export enum TransactionType {
  Buy = "buy",
  Sell = "sell"
}
