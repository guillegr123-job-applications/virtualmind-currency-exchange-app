import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import * as fromCurrencyRates from './currency-rates/currency-rates.reducer';
import { CurrencyRatesEffects } from './currency-rates/currency-rates.effects';
import { CurrencyRatesComponent } from './currency-rates/currency-rates.component';
import { CurrencyQuotationComponent } from './currency-quotation/currency-quotation.component';
import { CurrencyBuyComponent } from './currency-buy/currency-buy.component';
import * as fromCurrencyBuy from './currency-buy/currency-buy.reducer';
import { CurrencyBuyEffects } from './currency-buy/currency-buy.effects';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CurrencyRatesComponent,
    CurrencyQuotationComponent,
    CurrencyBuyComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'cotizaciones', component: CurrencyQuotationComponent },
      { path: 'compra', component: CurrencyBuyComponent }
    ]),
    StoreModule.forRoot({
      currencyRates: fromCurrencyRates.currencyRatesReducer,
      currencyBuy: fromCurrencyBuy.currencyRatesReducer
    }),
    EffectsModule.forRoot([CurrencyRatesEffects, CurrencyBuyEffects]),
    ToastNoAnimationModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
