import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { buyCurrency } from './currency-buy.actions';
import { BuyCurrency, BuyCurrencyResult } from './currency-buy.models';
import * as fromCurrencyBuy from './currency-buy.selectors';
import { AppState } from '../app.state';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-currency-buy',
  templateUrl: './currency-buy.component.html'
})
export class CurrencyBuyComponent {
  public supportedCurrencies = environment.currencies.join(', ');
  public supportedCurrenciesPattern = `(${environment.currencies.join('|')})`;

  public model = new BuyCurrency();
  public isFormVisible = true;
  public isLoading$: Observable<boolean>;
  public result$: Observable<BuyCurrencyResult>;
  public error$: Observable<string>;

  constructor(private store: Store<AppState>) {
    this.isLoading$ = this.store.select(fromCurrencyBuy.selectCurrencyBuyIsLoading);
    this.result$ = this.store.select(fromCurrencyBuy.selectCurrencyBuyResult);
    this.error$ = this.store.select(fromCurrencyBuy.selectError);

    this.isLoading$.subscribe(isLoading => {
      if (isLoading) {
        this.isFormVisible = false;
      }
    });
  }

  public submitTransaction(currencyBuyForm: NgForm) {
    if (!currencyBuyForm.valid) return;

    this.store.dispatch(
      buyCurrency({
        data: {
          ...this.model,
          amount: this.model.amount
        }
      })
    );
  }

  public showForm() {
    this.model = new BuyCurrency();
    this.isFormVisible = true;
  }
}
