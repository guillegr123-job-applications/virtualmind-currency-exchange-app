import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as moment from 'moment';
import { buyCurrency, buyCurrencySuccess, buyCurrencyFail } from './currency-buy.actions';
import { ApiResponse, ApiService, CurrencyTransactionResponseData, TransactionType } from '../api.service';

@Injectable()
export class CurrencyBuyEffects {

  constructor(private actions$: Actions, private apiService: ApiService) {
  }

  @Effect()
  buyCurrencyEffect = this.actions$.pipe(
    ofType(buyCurrency),
    mergeMap(({ data }) => {
      return this.apiService.putCurrencyTransaction(TransactionType.Buy, data).pipe(
        map((response: ApiResponse<CurrencyTransactionResponseData>) =>
          buyCurrencySuccess({
            data: {
              ...response.data,
              timestamp: moment(response.timestamp).toDate()
            }
           })
        ),
        catchError(response => of(buyCurrencyFail({
          error: (response.error && response.error.data && response.error.data.message) || 'Error!'
        })))
      )
    }),
  );
}
