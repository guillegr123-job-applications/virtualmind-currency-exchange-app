import { createAction, props } from "@ngrx/store";
import { BuyCurrency, BuyCurrencyResult } from './currency-buy.models';

export const buyCurrency = createAction(
  "[Currency Buy Component] Buy currency",
  props<{ data: BuyCurrency }>()
);

export const buyCurrencySuccess = createAction(
  "[Currency Buy Component] Buy currency success",
  props<{ data: BuyCurrencyResult }>()
);

export const buyCurrencyFail = createAction(
  "[Currency Buy Component] Buy currency fail",
  props<{ error: string }>()
);
