export class BuyCurrency {
  currencyCode: string;
  userId: string;
  amount: number;
}

export class BuyCurrencyResult extends BuyCurrency {
  id: string;
  rate: string;
  value: number;
  timestamp: Date;
}
