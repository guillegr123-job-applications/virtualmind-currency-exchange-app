import { createSelector } from '@ngrx/store';

import { AppState } from '../app.state';
import { CurrencyBuyState } from './currency-buy.reducer';

const selectCurrencyBuyState = (state: AppState) => state.currencyBuy;

export const selectCurrencyBuyResult = createSelector(
  selectCurrencyBuyState,
  (state: CurrencyBuyState) => state.result
);

export const selectCurrencyBuyIsLoading = createSelector(
  selectCurrencyBuyState,
  (state: CurrencyBuyState) => state.isLoading
);

export const selectError = createSelector(
  selectCurrencyBuyState,
  (state: CurrencyBuyState) => state.error
);
