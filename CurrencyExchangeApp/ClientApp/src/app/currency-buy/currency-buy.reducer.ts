import { Action, createReducer, on } from '@ngrx/store';
import { buyCurrency, buyCurrencySuccess, buyCurrencyFail } from './currency-buy.actions';
import { BuyCurrencyResult } from './currency-buy.models';

export interface CurrencyBuyState {
  isLoading: boolean,
  result: BuyCurrencyResult,
  error: string
}

const initialState: CurrencyBuyState = {
  isLoading: false,
  result: null,
  error: null
}

const _currencyRatesReducer = createReducer(
  initialState,
  on(buyCurrency, state => ({
    ...state,
    isLoading: true,
    result: null,
    error: null
  })),
  on(buyCurrencySuccess, (state: CurrencyBuyState, { data }) => ({
    ...state,
    isLoading: false,
    result: data
  })),
  on(buyCurrencyFail, (state: CurrencyBuyState, { error }) => ({
    ...state,
    isLoading: false,
    result: null,
    error
  }))
)

export function currencyRatesReducer(state: CurrencyBuyState | undefined, action: Action) {
  return _currencyRatesReducer(state, action);
}
