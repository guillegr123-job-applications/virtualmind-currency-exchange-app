import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';
import { AppState } from '../app.state';
import { CurrencyRate } from './currency-rates.models';
import { loadCurrencyRates } from './currency-rates.actions';
import { Observable } from 'rxjs';
import * as fromCurrencyRates from './currency-rates.selectors';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-currency-rates',
  templateUrl: './currency-rates.component.html'
})
export class CurrencyRatesComponent {
  public rates$: Observable<CurrencyRate[]>;
  public isLoading$: Observable<boolean>;
  public lastUpdate$: Observable<Date>;

  constructor(private store: Store<AppState>, toastr: ToastrService) {
    this.rates$ = this.store.select(fromCurrencyRates.selectCurrencyRates);
    this.isLoading$ = this.store.select(fromCurrencyRates.selectIsLoading);
    this.lastUpdate$ = this.store.select(fromCurrencyRates.selectLastUpdate);

    this.store.select(fromCurrencyRates.selectError)
      .subscribe(errorMessage => errorMessage && toastr.error('No se pudieron cargar las tasas de cambio de todas las monedas.'));
  }

  ngOnInit() {
    this.loadRates();
  }

  public loadRates() {
    environment.currencies.forEach(currencyCode =>
      this.store.dispatch(loadCurrencyRates({ currencyCode  }))
    );
  }
}
