import { createAction, props } from "@ngrx/store";
import { CurrencyRate } from './currency-rates.models';

export const loadCurrencyRates = createAction(
  "[Currency Rates Component] Load currency rates",
  props<{ currencyCode: string }>()
);

export const loadCurrencyRateSuccess = createAction(
  "[Currency Rates Component] Load currency rate success",
  props<{ currencyCode: string, rate: CurrencyRate}>()
);

export const loadCurrencyRateFail = createAction(
  "[Currency Rates Component] Load currency rate fail",
  props<{ currencyCode: string, error: string }>()
);
