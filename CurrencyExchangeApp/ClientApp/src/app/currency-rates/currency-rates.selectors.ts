import { createSelector } from '@ngrx/store';

import { AppState } from '../app.state';
import { CurrencyRatesState } from './currency-rates.reducer';

const selectCurrencyRatesState = (state: AppState) => state.currencyRates;

export const selectCurrencyRates = createSelector(
  selectCurrencyRatesState,
  (state: CurrencyRatesState) => Object.values(state.rates)
);

export const selectIsLoading = createSelector(
  selectCurrencyRatesState,
  (state: CurrencyRatesState) => state.loadingCounter !== 0
);

export const selectLastUpdate = createSelector(
  selectCurrencyRatesState,
  (state: CurrencyRatesState) => state.lastUpdate
);

export const selectError = createSelector(
  selectCurrencyRatesState,
  (state: CurrencyRatesState) => state.error
);
