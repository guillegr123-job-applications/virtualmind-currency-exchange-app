import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as moment from 'moment';
import { loadCurrencyRates, loadCurrencyRateSuccess, loadCurrencyRateFail } from './currency-rates.actions';
import { ApiResponse, ApiService, CurrencyRateData } from '../api.service';

@Injectable()
export class CurrencyRatesEffects {

  constructor(private actions$: Actions, private apiService: ApiService) {
  }

  @Effect()
  loadCurrencyRatesEffect = this.actions$.pipe(
    ofType(loadCurrencyRates),
    mergeMap(({ currencyCode }) => {
      return this.apiService.getCurrencyRate(currencyCode).pipe(
        map((response: ApiResponse<CurrencyRateData>) =>
          loadCurrencyRateSuccess({
            currencyCode,
            rate: {
              currencyCode,
              buy: response.data.buy,
              sell: response.data.sell,
              timestamp: moment(response.timestamp).toDate(),
              error: false
            }
          })
        ),
        catchError(response => of(loadCurrencyRateFail({
          currencyCode,
          error: (response.error && response.error.data && response.error.data.message) || 'Error!'
        })))
      )
    }),
  );
}
