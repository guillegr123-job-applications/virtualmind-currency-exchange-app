import { Action, createReducer, on } from '@ngrx/store';
import { loadCurrencyRates, loadCurrencyRateSuccess, loadCurrencyRateFail } from './currency-rates.actions';
import { CurrencyRate } from './currency-rates.models';

export interface CurrencyRatesState {
  rates: { [id: string]: CurrencyRate },
  loadingCounter: number,
  error: string,
  lastUpdate: Date
}

const initialState: CurrencyRatesState = {
  rates: {},
  loadingCounter: 0,
  error: null,
  lastUpdate: null
};

const _currencyRatesReducer = createReducer(
  initialState,
  on(loadCurrencyRates, state => ({
    ...state,
    loadingCounter: state.loadingCounter + 1,
    error: null
  })),
  on(loadCurrencyRateSuccess, (state: CurrencyRatesState, { currencyCode, rate }) => ({
    ...state,
    rates: {
      ...state.rates,
      [currencyCode]: rate
    },
    loadingCounter: state.loadingCounter - 1,
    lastUpdate: rate.timestamp > state.lastUpdate ? rate.timestamp : state.lastUpdate
  })),
  on(loadCurrencyRateFail, (state: CurrencyRatesState, { currencyCode, error }) => ({
    ...state,
    loadingCounter: state.loadingCounter - 1,
    rates: {
      ...state.rates,
      [currencyCode]: {
        ...state.rates[currencyCode],
        currencyCode,
        error: true
      }
    },
    error
  }))
);

export function currencyRatesReducer(state: CurrencyRatesState | undefined, action: Action) {
  return _currencyRatesReducer(state, action);
}
