export interface CurrencyRate {
  currencyCode: string;
  buy: string;
  sell: string;
  timestamp: Date;
  error: boolean;
};
