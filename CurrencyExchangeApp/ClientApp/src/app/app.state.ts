import { CurrencyRatesState } from "./currency-rates/currency-rates.reducer";
import { CurrencyBuyState } from './currency-buy/currency-buy.reducer';

export interface AppState {
  currencyRates: CurrencyRatesState,
  currencyBuy: CurrencyBuyState
};
