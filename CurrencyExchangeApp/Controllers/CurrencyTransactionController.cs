using System;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using CurrencyExchangeApp.Services.Resolvers;
using CurrencyExchangeApp.ViewModels;
using CurrencyExchangeApp.Data;
using CurrencyExchangeApp.Data.Models;

namespace CurrencyExchangeApp.Controllers
{
    /// <summary>
    /// Manages currency transactions.
    /// </summary>
    [ApiController]
    [Route("api/v1/transactions")]
    public class CurrencyTransactionController : AppControllerBase
    {
        private readonly ICurrencyRateServiceResolver _rateClientResolver;

        private readonly AppDbContext _dbContext;

        private readonly ILogger<CurrencyTransactionController> _logger;

        public CurrencyTransactionController(AppDbContext dbContext, ICurrencyRateServiceResolver rateClientResolver, ILogger<CurrencyTransactionController> logger)
        {
            _rateClientResolver = rateClientResolver;
            _dbContext = dbContext;
            _logger = logger;
        }

        /// <summary>
        /// Perform a currency exchange transaction.
        /// </summary>
        /// <param name="type">Transaction type.</param>
        /// <param name="transactionRequest">Transaction data.</param>
        /// <returns>Transaction confirmation details.</returns>
        /// <response code="200">Returns the transaction confirmation details.</response>
        /// <response code="400">Transaction data not valid.</response>
        /// <response code="404">Currency not supported.</response>
        /// <response code="429">Monthly limit exceeded.</response>
        /// <response code="500">An error occurred while trying to get currency rates.</response>
        [HttpPut("{type}")]
        [ProducesResponseType(typeof(ResponseDataModel<CurrencyTransactionResponseModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseDataModel<ErrorModel>), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ResponseDataModel<ErrorModel>), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ResponseDataModel<ErrorModel>), StatusCodes.Status429TooManyRequests)]
        [ProducesResponseType(typeof(ResponseDataModel<ErrorModel>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> PutTransaction(
            string @type,
            [FromBody] RequestDataModel<CurrencyTransactionRequestModel> transactionRequest
        )
        {
            CurrencyTransactionType currencyTransactionType;

            _logger.LogInformation(1606386734, "Requested transaction of type \"{0}\"", type);

            if (!Enum.TryParse<CurrencyTransactionType>(type, true, out currencyTransactionType))
            {
                _logger.LogWarning(1606386744, "Transaction type \"{0}\" not recognized", type);
                return NotFound($"Transaction type \"{type}\" not found.");
            }

            if (!ModelState.IsValid)
            {
                _logger.LogWarning(1606386754, "Invalid transaction data: {0}", string.Join(", ", ModelState.Keys));
                return Error("Invalid data.", StatusCodes.Status400BadRequest);
            }

            var transactionData = transactionRequest.Data;
            transactionData.CurrencyCode = transactionData.CurrencyCode.ToUpper();

            // Try to get rate
            var currencyCode = transactionData.CurrencyCode;

            _logger.LogDebug(1606387112, "Resolving rate service for currency \"{0}\"", currencyCode);

            var rateClient = _rateClientResolver.GetCurrencyRateService(currencyCode);

            if (rateClient == null)
            {
                _logger.LogWarning(
                    1606387136,
                    "Rate service not found for currency \"{0}\"",
                    currencyCode
                );
                return NotFound($"Currency code \"{currencyCode}\" not supported.");
            }

            _logger.LogInformation(1606387113, "Getting rate sfor currency \"{0}\"", currencyCode);

            var rates = await rateClient.GetRatesAsync(currencyCode);

            // Get and validate transaction limits
            var transactionType = currencyTransactionType.ToDataModelTransactionType();

            _logger.LogInformation(
                1606387114,
                "Getting limits for currency=\"{0}\" and transactionType={1}",
                currencyCode,
                transactionType
            );

            var limits = await _dbContext.TransactionLimits
                .SingleOrDefaultAsync(x => x.CurrencyCode == currencyCode && x.Type == transactionType);

            if (limits == null)
            {
                _logger.LogWarning(
                    1606387115,
                    "No limits found for currency=\"{0}\" and transactionType={1}",
                    currencyCode,
                    transactionType
                );
                return Error(
                    "The transaction cannot be applied, because no limits have been established"
                    + $" for \"{currencyCode}\"",
                    StatusCodes.Status501NotImplemented
                );
            }

            // Get user's month transaction total
            var transactionUserId = int.Parse(transactionData.UserId);
            var transactionDateTime = DateTime.UtcNow;

            _logger.LogInformation(
                1606387116,
                "Getting user month's total transactions for userId={0} and month={1}",
                transactionData.UserId,
                transactionDateTime.ToString("yyyy-MM")
            );

            var totalMonthTransactionValues = await _dbContext.CurrencyTransactions
                .Where(x =>
                    x.UserId == transactionUserId
                    && x.CurrencyCode == currencyCode
                    && x.Timestamp.Year == transactionDateTime.Year
                    && x.Timestamp.Month == transactionDateTime.Month
                )
                .SumAsync(x => x.Value);

            // Check monthly limit
            var transactionRate = currencyTransactionType == CurrencyTransactionType.Buy ? rates.Sell : rates.Buy;
            var transactionAmount = transactionData.Amount;
            var transactionValue = decimal.Round(transactionAmount / transactionRate, 2);   // Round to 2 decimal places, based on supported currencies
            
            _logger.LogInformation(
                1606387117,
                "Checking if userId={0} doesn't exceed monthly limit",
                transactionData.UserId
            );
            if (totalMonthTransactionValues + transactionValue >= limits.MontlyUserLimit)
            {
                var remainingValue = limits.MontlyUserLimit - totalMonthTransactionValues;

                _logger.LogWarning(
                    1606387118,
                    "Monthly limit exceeded for userId={0}. Additional data:"
                    + " transactionType={1}, currency={2}, transactionAmount={3},"
                    + " transactionValue={4}, monthlyLimit={5}, remaining={6}",
                    transactionData.UserId,
                    currencyTransactionType,
                    currencyCode,
                    transactionAmount,
                    transactionValue,
                    limits.MontlyUserLimit,
                    remainingValue
                );
                
                return Error(
                    "The transaction cannot be applied, because it will exceed the monthly user"
                    + $" limit for \"{currencyTransactionType}\" transactions for {currencyCode}."
                    + $"\n{currencyCode} {remainingValue} of {limits.MontlyUserLimit} remaining.",
                    StatusCodes.Status429TooManyRequests
                );
            }

            // Save transaction
            _logger.LogInformation(
                1606387120,
                "Transaction within permitted limit. Saving transaction..."
            );

            var transaction = new CurrencyTransaction
            {
                UserId = transactionUserId,
                Type = currencyTransactionType.ToDataModelTransactionType(),
                CurrencyCode = currencyCode,
                Amount = transactionAmount,
                Rate = transactionRate,
                Value = transactionValue,
                Timestamp = transactionDateTime
            };
            _dbContext.CurrencyTransactions.Add(transaction);
            await _dbContext.SaveChangesAsync();

            _logger.LogInformation(
                1606387121,
                "Transaction saved with Id={0}",
                transaction.Id
            );
            
            return Ok(
                new CurrencyTransactionResponseModel
                {
                    Id = transaction.Id.ToString(),
                    UserId = transaction.UserId.ToString(),
                    CurrencyCode = transaction.CurrencyCode,
                    Amount = transaction.Amount,
                    Rate = transaction.Rate.ToString(),
                    Value = transaction.Value,
                    Timestamp = transaction.Timestamp
                }
            );
        }
    }
}
