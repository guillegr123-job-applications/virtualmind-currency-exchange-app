using Microsoft.AspNetCore.Mvc;
using CurrencyExchangeApp.ViewModels;
using Microsoft.AspNetCore.Http;

namespace CurrencyExchangeApp.Controllers
{
    public abstract class AppControllerBase : ControllerBase
    {
        protected OkObjectResult Ok<TData>(TData data)
            where TData : IViewModel
        {
            var responseModel = data.ToResponseDataModel(HttpContext.TraceIdentifier);
            return base.Ok(responseModel);
        }

        protected NotFoundObjectResult NotFound(string errorMessage)
        {
            var errorModel = new ErrorModel
            {
                Message = errorMessage
            };
            var responseModel = errorModel.ToResponseDataModel(HttpContext.TraceIdentifier);
            return base.NotFound(responseModel);
        }

        protected ObjectResult Error(string errorMessage, int statusCode)
        {
            var errorModel = new ErrorModel
            {
                Message = errorMessage
            };
            var responseModel = errorModel.ToResponseDataModel(HttpContext.TraceIdentifier);
            return base.StatusCode(statusCode, responseModel);
        }
    }
}