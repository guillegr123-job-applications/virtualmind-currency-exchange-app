using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CurrencyExchangeApp.Services.Resolvers;
using CurrencyExchangeApp.ViewModels;

namespace CurrencyExchangeApp.Controllers
{
    /// <summary>
    /// Queries transaction rates.
    /// </summary>
    [ApiController]
    [Route("api/v1/currencies")]
    public class CurrencyRateController : AppControllerBase
    {
        private readonly ICurrencyRateServiceResolver _rateServiceResolver;

        private readonly ILogger<CurrencyRateController> _logger;

        public CurrencyRateController(ICurrencyRateServiceResolver rateClientResolver, ILogger<CurrencyRateController> logger)
        {
            _rateServiceResolver = rateClientResolver;
            _logger = logger;
        }

        /// <summary>
        /// Get the most current rates for a specific currency.
        /// </summary>
        /// <param name="currency">Currency alpha code (ISO 4217).</param>
        /// <returns>Currency rate information.</returns>
        /// <response code="200">Returns the currency rate information.</response>
        /// <response code="404">Currency not supported.</response>
        /// <response code="500">An error occurred while trying to get the currency rates.</response>
        [HttpGet("{currency}/rates")]
        [ProducesResponseType(typeof(ResponseDataModel<CurrencyRateModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseDataModel<ErrorModel>), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(ResponseDataModel<ErrorModel>), StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetRatesAsync(string currency)
        {
            _logger.LogInformation(1606381407, "Requested rate for currency \"{0}\"", currency);
            var currencyCode = currency.ToUpper();

            _logger.LogDebug(1606381408, "Resolving rate service for currency \"{0}\"", currency);
            var rateService = _rateServiceResolver.GetCurrencyRateService(currencyCode);

            if (rateService == null)
            {
                _logger.LogWarning(
                    1606381420,
                    "Rate service not found for currency \"{0}\"",
                    currency
                );
                return NotFound($"Currency code \"{currencyCode}\" not supported.");
            }

            _logger.LogInformation(
                1606381421,
                "Rate service of type {0} found for currency \"{1}\"",
                rateService.GetType().FullName, currency
            );
            _logger.LogDebug(
                1606381657,
                "Getting rates from service for currency \"{0}\"",
                currency
            );
            var rates = await rateService.GetRatesAsync(currencyCode);
            
            _logger.LogInformation(
                1606381667,
                "Rates obtained for currency \"{0}\": Sell={1}, Buy={2}, LastUpdateTimestamp={3}",
                currency, rates.Sell, rates.Buy, rates.LastUpdateTimestamp
            );
            return Ok(rates.ToCurrencyRateModel());
        }
    }
}
