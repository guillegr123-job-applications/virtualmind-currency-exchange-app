using System.Linq;
using CurrencyExchangeApp.Data.Models;

namespace CurrencyExchangeApp.Data.Fixtures
{
    public static class DevelopmentFixtures
    {
        public static void Load(AppDbContext dbContext)
        {
            if (dbContext.TransactionLimits.Count() > 0)
            {
                return;
            }

            dbContext.TransactionLimits.AddRange(
                new TransactionLimit
                {
                    CurrencyCode = "USD",
                    Type = TransactionType.Buy,
                    MontlyUserLimit = 200
                },
                new TransactionLimit
                {
                    CurrencyCode = "BRL",
                    Type = TransactionType.Buy,
                    MontlyUserLimit = 300
                }
            );

            dbContext.SaveChanges();
        }
    }
}