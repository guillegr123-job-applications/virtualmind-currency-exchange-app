using System;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using CurrencyExchangeApp.Data.Models;

namespace CurrencyExchangeApp.Data
{
    public class AppDbContext : DbContext
    {
        private readonly Expression<Func<TransactionType, string>> transactionTypeToString = 
            type => type == TransactionType.Buy ? "B": "S";
        private readonly Expression<Func<string, TransactionType>> stringToTransactionType = 
            stringType => stringType == "B" ? TransactionType.Buy : TransactionType.Sell;

        public DbSet<CurrencyTransaction> CurrencyTransactions { get; set; }
        public DbSet<TransactionLimit> TransactionLimits { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TransactionLimit>(b => {
                b.ToTable("TransactionLimit");

                b.HasKey(x => x.Id);

                b.Property(x => x.Id)
                    .UseIdentityColumn();

                b.Property(x => x.CurrencyCode)
                    .HasMaxLength(3)
                    .IsFixedLength()
                    .IsUnicode(false)
                    .IsRequired();

                b.Property(x => x.Type)
                    .HasConversion<string>(transactionTypeToString, stringToTransactionType)
                    .HasMaxLength(1)
                    .IsFixedLength()
                    .IsUnicode(false)
                    .IsRequired();

                b.Property(x => x.MontlyUserLimit)
                    .HasPrecision(19, 4)
                    .IsRequired();

                b.HasIndex(x => new { x.CurrencyCode, x.Type });
            });

            modelBuilder.Entity<CurrencyTransaction>(b => {
                b.ToTable("CurrencyTransaction");

                b.HasKey(x => x.Id);

                b.Property(x => x.Id)
                    .UseIdentityColumn();

                b.Property(x => x.Type)
                    .HasConversion<string>(transactionTypeToString, stringToTransactionType)
                    .HasMaxLength(1)
                    .IsFixedLength()
                    .IsUnicode(false)
                    .IsRequired();

                b.Property(x => x.UserId)
                    .IsRequired();

                b.Property(x => x.CurrencyCode)
                    .HasMaxLength(3)
                    .IsFixedLength()
                    .IsUnicode(false)
                    .IsRequired();

                b.Property(x => x.Amount)
                    .HasPrecision(19, 4)
                    .IsRequired();

                b.Property(x => x.Rate)
                    .HasPrecision(29, 14)
                    .IsRequired();

                b.Property(x => x.Value)
                    .HasPrecision(19, 4)
                    .IsRequired();

                b.Property(x => x.Timestamp)
                    .IsRequired();

                b.HasIndex(x => new { x.UserId, x.CurrencyCode });
                b.HasIndex(x => x.Timestamp);
            });
        }
    }
}
