using System;

namespace CurrencyExchangeApp.Data.Models
{
    public class CurrencyTransaction
    {
        public long Id { get; set; }
        public TransactionType Type { get; set; }
        public int UserId { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public decimal Value { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
