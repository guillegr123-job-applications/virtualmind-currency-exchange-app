using System;

namespace CurrencyExchangeApp.Data.Models
{
    public class TransactionLimit
    {
        public int Id { get; set; }
        public TransactionType Type { get; set; }
        public string CurrencyCode { get; set; }
        public decimal MontlyUserLimit { get; set; }
    }
}
