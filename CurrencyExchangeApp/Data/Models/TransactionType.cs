namespace CurrencyExchangeApp.Data.Models
{
    public enum TransactionType {
        Buy, Sell
    }
}
