using System;

namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Transaction response data.
    /// </summary>
    public class CurrencyTransactionResponseModel : CurrencyTransactionRequestModel
    {
        /// <summary>
        /// Transaction ID.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Transaction trade rate.
        /// </summary>
        public string Rate { get; set; }

        /// <summary>
        /// Value traded, in foreign currency.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Transaction timestamp.
        /// </summary>
        public DateTime Timestamp { get; set; }
        
    }
}
