namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Interface to identify view models during build time, for type safety.
    /// </summary>
    public interface IViewModel
    {
    }
}
