using System;
using CurrencyExchangeApp.Data.Models;
using CurrencyExchangeApp.Services.Models;

namespace CurrencyExchangeApp.ViewModels
{
    public static class ViewModelExtensions
    {
        public static ResponseDataModel<TData> ToResponseDataModel<TData>(this TData data, string requestId)
            where TData: IViewModel
            => new ResponseDataModel<TData> {
                Data = data,
                RequestId = requestId,
                Timestamp = DateTime.UtcNow
            };

        public static CurrencyRateModel ToCurrencyRateModel(this ServiceCurrencyRateModel externalRateModel) =>
            new CurrencyRateModel
            {
                Buy = externalRateModel.Buy.ToString(),
                Sell = externalRateModel.Sell.ToString(),
                LastUpdateTimestamp = externalRateModel.LastUpdateTimestamp
            };

        public static TransactionType ToDataModelTransactionType(this CurrencyTransactionType type) =>
            (TransactionType)Enum.Parse(typeof(TransactionType), type.ToString());
    }
}
