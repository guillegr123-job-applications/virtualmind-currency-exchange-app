namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Error data.
    /// </summary>
    public class ErrorModel : IViewModel
    {
        /// <summary>
        /// Error message.
        /// </summary>
        public string Message { get; set; }
    }
}
