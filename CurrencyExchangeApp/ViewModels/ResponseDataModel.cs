using System;

namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Response wrapper.
    /// </summary>
    public class ResponseDataModel<TData> : RequestDataModel<TData>
        where TData: IViewModel
    {
        /// <summary>
        /// Request unique ID.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Operation timestamp.
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
