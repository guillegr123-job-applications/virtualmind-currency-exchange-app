namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Currency transaction type.
    /// </summary>
    public enum CurrencyTransactionType {
        /// <summary>
        /// Buy currency.
        /// </summary>
        Buy
        // Sell // TODO: Not supported
    }
}
