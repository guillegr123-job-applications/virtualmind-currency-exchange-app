using System;

namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Currency rates data.
    /// </summary>
    public class CurrencyRateModel : IViewModel
    {

        /// <summary>
        /// Currency sell rate.
        /// </summary>
        public string Sell { get; set; }


        /// <summary>
        /// Currency buy rate.
        /// </summary>
        public string Buy { get; set; }

        /// <summary>
        /// Date and time when the rates where updated.
        /// </summary>
        public DateTime LastUpdateTimestamp { get; set; }
    }
}
