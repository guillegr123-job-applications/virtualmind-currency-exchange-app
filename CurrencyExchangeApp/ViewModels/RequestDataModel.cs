using System;

namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Request wrapper.
    /// </summary>
    public class RequestDataModel<TData>
        where TData: IViewModel
    {
        /// <summary>
        /// Data.
        /// </summary>
        public TData Data { get; set; }
    }
}
