namespace CurrencyExchangeApp.ViewModels
{
    /// <summary>
    /// Transaction request data.
    /// </summary>
    public class CurrencyTransactionRequestModel : IViewModel
    {
        /// <summary>
        /// User ID.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Currency alpha code (ISO 4217).
        /// </summary>
        public string CurrencyCode { get; set; }

        /// <summary>
        /// Amount of money to trade, in local currency.
        /// </summary>
        public decimal Amount { get; set; }
    }
}
