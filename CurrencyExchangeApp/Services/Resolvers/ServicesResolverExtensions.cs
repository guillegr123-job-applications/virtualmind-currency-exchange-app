using System.Linq;
using System.Net.Http;
using System.Reflection;
using CurrencyExchangeApp.Configuration.Models;
using CurrencyExchangeApp.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CurrencyExchangeApp.Services.Resolvers
{
    public static class ServicesResolverExtensions
    {
        public static void RegisterServicesClients(this IServiceCollection services, string ns, IConfiguration configuration)
        {
            if (configuration != null)
            {
                services.Configure<ServicesConfiguration>(configuration.GetSection("ExternalServices"));
            }

            var currencyRateClients = Assembly.GetCallingAssembly().GetTypes()
                .Where(t =>
                    t.IsClass 
                    && t.Namespace == ns
                    && typeof(ICurrencyRateService).IsAssignableFrom(t)
                )
                .ToArray();
            foreach (var clientType in currencyRateClients)
            {
                services.AddSingleton(clientType);
            }
            services.AddTransient<HttpClient>();

            services.AddSingleton<ICurrencyRateServiceResolver>(provider =>
                new CurrencyRateServiceResolver(provider, currencyRateClients)
            );
        }
    }
}