using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using CurrencyExchangeApp.Services.Interfaces;

namespace CurrencyExchangeApp.Services.Resolvers
{
    public class CurrencyRateServiceResolver : ICurrencyRateServiceResolver
    {
        private readonly ILogger<CurrencyRateServiceResolver> _logger;
        private readonly Lazy<IDictionary<string, ICurrencyRateService>> _clientTypesByCurrency;

        public CurrencyRateServiceResolver(IServiceProvider provider, IEnumerable<Type> types)
        {
            _logger = (ILogger<CurrencyRateServiceResolver>)provider.GetService(typeof(ILogger<CurrencyRateServiceResolver>));
            _clientTypesByCurrency = new Lazy<IDictionary<string, ICurrencyRateService>>(
                () =>
                types.SelectMany(type => {
                    var serviceClient = provider.GetService(type) as ICurrencyRateService;

                    _logger.LogInformation(
                        1606408197,
                        "Registering service client of type \"{0}\" for currencies [{1}]",
                        type.FullName,
                        string.Join(", ", serviceClient.CurrencyCodes)
                    );

                    return serviceClient.CurrencyCodes.ToDictionary(c => c, c => serviceClient);
                })
                .ToDictionary(kv => kv.Key, kv => kv.Value)
            );
        }

        public ICurrencyRateService GetCurrencyRateService(string currencyCode)
        {
            ICurrencyRateService service;

            _logger.LogInformation(
                1606408180,
                "Attempting to get service client for currency \"{0}\"",
                currencyCode
            );

            if (_clientTypesByCurrency.Value.TryGetValue(currencyCode, out service))
            {
                _logger.LogInformation(
                    1606408181,
                    "Obtained service client of type \"{0}\" for currency \"{1}\"",
                    service.GetType().FullName,
                    currencyCode
                );
                return service;
            }

            _logger.LogInformation(
                1606408182,
                "Service client not found for currency \"{0}\"",
                currencyCode
            );
            return null;
        }
    }
}