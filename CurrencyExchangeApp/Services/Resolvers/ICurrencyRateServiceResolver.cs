using CurrencyExchangeApp.Services.Interfaces;

namespace CurrencyExchangeApp.Services.Resolvers
{
    public interface ICurrencyRateServiceResolver
    {
        ICurrencyRateService GetCurrencyRateService(string currencyCode);
    }
}