using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CurrencyExchangeApp.Services.Interfaces;
using CurrencyExchangeApp.Services.Models;
using System.Text.RegularExpressions;
using System.Globalization;
using CurrencyExchangeApp.Configuration.Models;
using Microsoft.Extensions.Options;

namespace CurrencyExchangeApp.Services.Clients
{
    public class BancoProvinciaRateService: ICurrencyRateService
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<BancoProvinciaRateService> _logger;
        private readonly string _serviceUrl;

        public string[] CurrencyCodes => new string[] { "USD" };

        public BancoProvinciaRateService(
            HttpClient httpClient,
            ILogger<BancoProvinciaRateService> logger,
            IOptions<ServicesConfiguration> config
        )
        {
            _httpClient = httpClient;
            _logger = logger;
            _serviceUrl = config.Value.BancoProvinciaUrl;
        }

        public async Task<ServiceCurrencyRateModel> GetRatesAsync(string currencyCode)
        {
            var urlCurrencyCode = "Dolar";
            var url = $"{_serviceUrl}/{urlCurrencyCode}";

            _logger.LogInformation(
                1606382196,
                "Getting rates for currency=\"{0}\" from URI=\"{1}\"",
                currencyCode,
                url
            );

            var rateResponse = await _httpClient.GetAsync(url);
            var rateResponseContent = await rateResponse.Content.ReadAsStringAsync();

            _logger.LogDebug(
                1606382444,
                "Obtained rate response from service: {0}",
                rateResponseContent
            );

            var rateData = JsonConvert.DeserializeObject<string[]>(rateResponseContent);

            _logger.LogInformation(
                1606382454,
                "Rate data deserialized",
                rateResponseContent
            );

            _logger.LogDebug(
                1606382464,
                "Trying to get LastUpdateTimestamp from service response"
            );
            DateTime lastUpdateTimestamp = DateTime.UtcNow;
            var lastUpdateTimestampRegex = new Regex("(?<day>\\d{1,2})\\/(?<month>\\d{1,2})\\/(?<year>\\d{4}) (?<hours>\\d{1,2})\\:(?<minutes>\\d{2})");
            var lastUpdateTimestampMatch = lastUpdateTimestampRegex.Match(rateData[2]);
            var isTimestampParsed = lastUpdateTimestampMatch.Success
                && DateTime.TryParseExact(
                    lastUpdateTimestampMatch.Value,
                    "d/M/yyyy HH:mm",
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out lastUpdateTimestamp
                );
            if (isTimestampParsed)
            {
                var tz = TimeZoneInfo.FindSystemTimeZoneById("America/Buenos_Aires");
                lastUpdateTimestamp = lastUpdateTimestamp.Add(tz.BaseUtcOffset);
                _logger.LogInformation(
                    1606382474,
                    "LastUpdateTimestamp parsed properly from service response: {0}",
                    lastUpdateTimestamp.ToString("o")
                );
            }
            else
            {
                _logger.LogInformation(
                    1606382484,
                    "LastUpdateTimestamp not parsed from service response. Using current date and time: {0}",
                    lastUpdateTimestamp.ToString("o")
                );
            }

            return new ServiceCurrencyRateModel
            {
                Buy = decimal.Parse(rateData[0]),
                Sell = decimal.Parse(rateData[1]),
                LastUpdateTimestamp = lastUpdateTimestamp
            };
        }
    }
}
