using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using CurrencyExchangeApp.Services.Interfaces;
using CurrencyExchangeApp.Services.Models;

namespace CurrencyExchangeApp.Services.Clients
{
    public class DummyRealRateClient: ICurrencyRateService
    {
        private readonly BancoProvinciaRateService _exchangeClient;
        private readonly ILogger<DummyRealRateClient> _logger;

        public string[] CurrencyCodes => new string[] { "BRL" };

        public DummyRealRateClient(BancoProvinciaRateService exchangeClient, ILogger<DummyRealRateClient> logger)
        {
            _exchangeClient = exchangeClient;
            _logger = logger;
        }

        public async Task<ServiceCurrencyRateModel> GetRatesAsync(string currencyCode)
        {
            var rates = await _exchangeClient.GetRatesAsync(currencyCode);

            _logger.LogInformation(
                1606389766,
                "Calculating dummy rates for currency=\"{0}\"",
                currencyCode
            );

            return new ServiceCurrencyRateModel
            {
                Buy = rates.Buy/4,
                Sell = rates.Sell/4,
                LastUpdateTimestamp = rates.LastUpdateTimestamp
            };
        }
    }
}
