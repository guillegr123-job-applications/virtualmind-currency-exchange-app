using System.Threading.Tasks;
using CurrencyExchangeApp.Services.Models;

namespace CurrencyExchangeApp.Services.Interfaces
{
    public interface ICurrencyRateService
    {
        string[] CurrencyCodes { get; }

        Task<ServiceCurrencyRateModel> GetRatesAsync(string currencyCode);
    }
}
