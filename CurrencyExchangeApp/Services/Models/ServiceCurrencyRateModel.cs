using System;

namespace CurrencyExchangeApp.Services.Models
{
    public class ServiceCurrencyRateModel
    {
        public decimal Sell { get; set; }

        public decimal Buy { get; set; }
        
        public DateTime LastUpdateTimestamp { get; set; }
    }
}
