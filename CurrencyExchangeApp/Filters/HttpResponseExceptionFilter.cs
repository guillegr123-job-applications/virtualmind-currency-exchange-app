using System;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using CurrencyExchangeApp.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace CurrencyExchangeApp.Filters
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        private readonly ILogger<HttpResponseExceptionFilter> _logger;

        public int Order { get; } = int.MaxValue - 10;

        public HttpResponseExceptionFilter(ILogger<HttpResponseExceptionFilter> logger)
        {
            _logger = logger;
        }

        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context)
        {            
            if (context.Exception is Exception exception)
            {
                _logger.LogError(1606390324, exception, "Exception detected");

                var errorModel = new ErrorModel
                {
                    Message = exception.Message
                };
                var responseModel = errorModel.ToResponseDataModel(context.HttpContext.TraceIdentifier);
                context.Result = new ObjectResult(responseModel)
                {
                    StatusCode = StatusCodes.Status500InternalServerError //context.Exception.Status,
                };
                context.ExceptionHandled = true;
            }
        }
    }
}
