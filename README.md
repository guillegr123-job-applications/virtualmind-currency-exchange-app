# Currency Exchange App

## Build solution
To build all the projects in the solution using the .NET CLI, run the following command in the root folder:

```sh
dotnet run
```

## Run tests
To run all the tests for the solution, execute the following command:

```sh
dotnet test
```

## Run API and UI in development mode
To run the API and UI locally in development mode, it is necessary first to set the development database connection string for a SQL Server instance at the file `CurrencyExchangeApp/appsettings.Development.json`, in the property `CurrencyExchangeDb`. If it is necessary, also set the external services URLs at the `ExternalServices` property (you can check the `CurrencyExchangeApp/appsettings` files for the default configuration).

After setting up the configuration, we can execute the following command:

```sh
dotnet run -p CurrencyExchangeApp/CurrencyExchangeApp.csproj
```

The Angular SPA will be available at: https://localhost:5001
The Swagger UI for the API will be available at: https://localhost:5001/swagger/index.html

**NOTE:** The database is created on the fly, and limits are pre-populated in the first run, thanks to Entity Framework's code first, using the specified connection string.

## Logging
The log levels are set at the appsettings files. The main log configuration is set at the file `CurrencyExchangeApp/nlog.config`. The logs are generated in the executing directory, under a `Logs` folder, named with the current UTC date when the file was generated. When running the application locally in development mode, the logs are generated at `CurrencyExchangeApp/bin/Debug/netcoreapp3.1/Logs`.

## Special considerations

- Currently supported exchange currencies are US Dollar (USD) and Brazilian Real (BRL), in ISO alpha code.
- For rates, the `string` type was used instead of `decimal` in the API models because rates may have up to 14 decimal digits (VND to XBT), and such precision is not supported by Javascript's `Number` type because it is stored using a floating point representation.
- A `long` integer has been used for the transaction ID, because transactions can generate a large amount of entries. An improvement over this would be to use `Guid` as keys (identifiers), in order to also allow this web application to scale horizontally by avoiding conflicts between auto-generated correlative numbers.
- Entity identifiers in the API models are also represented by `string` types, because of Javascript's limited `Number` range (considering the previous statement), and to keep a standard across the API.
- Although the requirements stated that the buy transactions should register the calculated value in the database, I decided to also store the original amount and current trading rate for tracing purposes.

## To improve

- Although the integration tests do test the API as a whole, it would be better to have unit tests for each class, to be able to identify quickly any error or regression related to a specific component.
- Extract common re-usable values from tests into a constants file.
- Cache USD rates, to prevent anothe request when a dependant currency rate is requested at the same time.
- Get list of available currency codes from the API, and list them in a dropdown in the buy currency form at the UI
- User registration and authentication.
- Generate a signed and encrypted JSON Web Token for the user on login, which will be sent in the following requests for authorization to perform delicate operations such as currency transactions. In this way, the user authenticity can be verified thanks to the token signature. Also, due to the encryption, no sensitive data (i.e. the userId) is exposed to the client or third parties.
- Use error codes, to be able to show better error messages to the user, by allowing the client app to tell apart what kind of error happens.
- Extract service interfaces into new project, so they can be implemented by another teams for new services, as libraries. These libraries could even be loaded dynamically to be able to support new services easily.
- Format amounts according to each currency at the UI.